import 'package:flutter/material.dart';
import 'package:trip/model/list.dart';

List<CategoryModel> getcategories() {
  List<CategoryModel> category = new List<CategoryModel>();
  CategoryModel categoryModel;

  categoryModel = new CategoryModel();
  categoryModel.categoryname = "Language";
  categoryModel.imageUrl = "assets/images/language.png";
  categoryModel.icon = Icon(
    Icons.arrow_forward_ios,
    color: Color(0xffBB86FC),
    size: 13,
  );
  category.add(categoryModel);
  categoryModel = new CategoryModel();
  categoryModel.categoryname = "Rate Us";
  categoryModel.imageUrl = "assets/images/rating.png";
  categoryModel.icon = null;
  category.add(categoryModel);
  categoryModel = new CategoryModel();
  categoryModel.categoryname = "Share Us";
  categoryModel.imageUrl = "assets/images/share.png";
  categoryModel.icon = null;
  category.add(categoryModel);
  categoryModel = new CategoryModel();
  categoryModel.categoryname = "Call Us";
  categoryModel.imageUrl = "assets/images/call.png";
  categoryModel.icon = null;
  category.add(categoryModel);
  categoryModel = new CategoryModel();
  categoryModel.categoryname = "How to use?";
  categoryModel.imageUrl = "assets/images/how-to-use.png";
  categoryModel.icon = null;
  category.add(categoryModel);
  categoryModel = new CategoryModel();
  categoryModel.categoryname = "Report a Problem";
  categoryModel.imageUrl = "assets/images/report.png";
  categoryModel.icon = null;
  category.add(categoryModel);
  categoryModel = new CategoryModel();
  categoryModel.categoryname = "Teams and Policy";
  categoryModel.imageUrl = "assets/images/done.png";
  categoryModel.icon = null;
  category.add(categoryModel);
  categoryModel = new CategoryModel();
  categoryModel.categoryname = "Analytics";
  categoryModel.imageUrl = "assets/images/statistics.png";
  categoryModel.icon = Icon(
    Icons.arrow_forward_ios,
    color: Color(0xffBB86FC),
    size: 13,
  );
  category.add(categoryModel);
  categoryModel = new CategoryModel();
  categoryModel.categoryname = "Done Deals";
  categoryModel.imageUrl = "assets/images/list.png";
  categoryModel.icon = null;
  category.add(categoryModel);

  return category;
}
