import 'package:flutter/material.dart';

class UpperCard extends StatelessWidget {
  final double _height;
  UpperCard(this._height);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 15, 20, 5),
      child: Container(
        height: _height * 0.15,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(color: Colors.grey[300], blurRadius: 2, offset: Offset(3, 5)),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 10, 10, 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  buildText(
                    'Current balance:',
                    Color(0xff6225EE),
                  ),
                  buildText(
                    '4,423 Riyal',
                    Color(0xff3B566E),
                  ),
                  buildText(
                    'App Fee:',
                    Color(0xff6225EE),
                  ),
                  buildText(
                    '884.6 Riyal',
                    Color(0xff3B566E),
                  ),
                ],
              ),
              RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(7)),
                child: Text(
                  'Cash Out',
                  style: TextStyle(color: Colors.white),
                ),
                color: Color(0xff6225EE),
                onPressed: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }

  Text buildText(String text, Color color) {
    return Text(
      text,
      style: TextStyle(fontSize: 16, color: color, fontWeight: FontWeight.bold),
    );
  }
}
