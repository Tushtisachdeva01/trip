import 'package:flutter/material.dart';

class IconRow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Image.asset('assets/images/facebook.png'),
        Image.asset('assets/images/insta.png'),
        Image.asset('assets/images/snapchat.png'),
      ],
    );
  }
}
