import 'package:flutter/material.dart';
import 'package:trip/helper/list_data.dart';
import 'package:trip/model/list.dart';

class LowerCard extends StatefulWidget {
  final double _height;
  LowerCard(this._height);

  @override
  _LowerCardState createState() => _LowerCardState();
}

class _LowerCardState extends State<LowerCard> {
  List<CategoryModel> categories = new List<CategoryModel>();
  @override
  void initState() {
    super.initState();
    categories = getcategories();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 15, 20, 5),
      child: Container(
        height: widget._height * 0.55,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(20),
          boxShadow: [
            BoxShadow(
                color: Colors.grey[300], blurRadius: 2, offset: Offset(3, 5)),
          ],
        ),
        child: ListView.separated(
          padding: EdgeInsets.all(0),
          itemBuilder: (context, index) => Padding(
            padding: const EdgeInsets.all(0),
            child: GestureDetector(
              onTap: () {
                print('hi');
              },
              child: ListTile(
                dense: true,
                leading: Image.asset(categories[index].imageUrl),
                title: Text(
                  categories[index].categoryname,
                  style: TextStyle(fontSize: 13),
                ),
                trailing: categories[index].icon,
              ),
            ),
          ),
          separatorBuilder: (context, index) => Divider(
            color: Color(0xffD8D8D8),
            thickness: 1,
            indent: 10,
            endIndent: 10,
            height: 2,
          ),
          itemCount: categories.length,
        ),
      ),
    );
  }
}
