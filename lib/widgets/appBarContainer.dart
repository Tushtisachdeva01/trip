import 'package:flutter/material.dart';

class AppBarConatiner extends StatelessWidget {
  final double _height;
  AppBarConatiner(this._height);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: _height * 0.26,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.bottomLeft,
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                Color(0xff6225EE),
                Color(0xff8D53F5),
              ]),
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(20)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  blurRadius: 5,
                  offset: Offset(0, 5)
                ),
              ],
            ),
            margin: const EdgeInsets.fromLTRB(10, 0, 10, 20),
          ),
          Container(
            height: 115,
            width: 115,
            decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
                Color(0xff6225EE),
                Color(0xff692DEF),
              ]),
              borderRadius: BorderRadius.circular(100),
            ),
            margin: const EdgeInsets.symmetric(horizontal: 30),
            child: Center(
              child: CircleAvatar(
                backgroundImage: AssetImage('assets/images/profile.png'),
                radius: 50,
              ),
            ),
          ),
          Positioned(
            bottom: 50,
            left: 160,
            child: Text(
              'Ahmed Al-Kheerow',
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          ),
          Positioned(
            bottom: 30,
            left: 160,
            child: Text(
              'ID: 93956',
              style: TextStyle(color: Colors.white, fontSize: 14),
            ),
          ),
          Positioned(
            top: 50,
            right: 20,
            child: Image.asset('assets/images/setting.png'),
          ),
        ],
      ),
    );
  }
}
