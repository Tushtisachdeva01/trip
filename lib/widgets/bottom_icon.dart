import 'package:flutter/material.dart';

class BottomRow extends StatelessWidget {
  final double _height;
  BottomRow(this._height);
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: GestureDetector(
        onTap: () {},
        child: Container(
          width: MediaQuery.of(context).size.width * 0.7,
          height: _height * 0.05,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            gradient: LinearGradient(
              colors: [
                Color(0xffF65441),
                Color(0xffFC7656),
              ],
            ),
          ),
          child: Align(
            alignment: Alignment.center,
            child: Text(
              'Logout',
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
          ),
        ),
      ),
    );
  }
}
