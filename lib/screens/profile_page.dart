import 'package:flutter/material.dart';
import 'package:trip/widgets/appBarContainer.dart';
import 'package:trip/widgets/bottom_icon.dart';
import 'package:trip/widgets/icon_row.dart';
import 'package:trip/widgets/lower_card.dart';
import 'package:trip/widgets/upper_card.dart';

class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    final _height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Column(
        children: <Widget>[
          AppBarConatiner(_height),
          Container(
            height: _height * 0.65,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  UpperCard(_height),
                  Padding(
                    padding: const EdgeInsets.only(left: 25, top: 10),
                    child: Text(
                      'My Account',
                      style: TextStyle(
                        color: Color(0xff6225EE),
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  LowerCard(_height),
                  SizedBox(height: 30),
                  IconRow(),
                  SizedBox(height: 30),
                  BottomRow(_height),
                  SizedBox(height: 30),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
