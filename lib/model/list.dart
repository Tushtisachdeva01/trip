import 'package:flutter/material.dart';

class CategoryModel {
  String categoryname;
  String imageUrl;
  Icon icon;

  CategoryModel({this.categoryname,this.imageUrl,this.icon});
}